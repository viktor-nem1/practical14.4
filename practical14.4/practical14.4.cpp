﻿#include <iostream>

int main() 
{
	std::string name = "Hello, World!";
	std::cout << "String variable: " << name << std::endl;
	std::cout << "String length: " << name.length() << std::endl;
	std::cout << "First character: " << name.front() << std::endl;
	std::cout << "Last character: " << name.back() << std::endl;
	return 0;
}
